Soundscape Analysis
=======================
Training SVM Classifiers using soundscape audio data.

REQUIREMENTS
------------

 * pyAudioAnalysis Library (https://github.com/tyiannak/pyAudioAnalysis)
   In order to be able to call the pyAudioAnalysis library from any path you need to
   add the folder that contains it in the ~/.bashrc file. In particular, add a line as
   the follow in ~/.bashrc:
       export PYTHONPATH=$PYTHONPATH:"/home/path/to/pyAudioAnalysis/"


How To Use The Library
-----------------------
In order to train the SVM Classifiers all audio files need to be put in the same directory. 
The recommended path for the directory would be the same with this README file.
The only python script you need to import to your python code is 'soundscape.py'.
This script consists of two methods:

    1.  trainsvm(wavFolder, db, NumberOfIterations): train SVM Classifiers. 
        Using trainsvm() with the appropriate parameters, svm Classifiers can be
        trained using the folder of wav files and the corresponding database 
        file (.json file). If validating the classifiers is desired, then 
        NumberOfIterations must have a positive value which is corresponding 
        to the iterative splitting to train and validation data. Otherwise, 
        NumberOfIterations has to be zero. Note that the trained svm files will be
        stored in 'svmClassifiers' folder and the validation results will be  stored  
        in the 'validation_results' folder.       

    2.  applysvm(testdata, svmfolder): test the trained classifiers with new data.
        testdata can be either a folder of audio files or a single audio file. 
        svmfolder is the folder containing the svm files of the trained svm
        classifiers. An example of this folder is 'defaultClassifiers' folder.  


Examples
-----------------------
The following examples have to be run in the same folder with the python scripts.

    $ python
    >> import soundscape
    >> soundscape.trainsvm("../myFolderofAudioFiles","../data.json",3) 
    >> soundscape.applysvm("../test1.wav","../defaultClassifiers/")
    >> soundscape.applysvm("../testfolder/","../svmClassifiers/")
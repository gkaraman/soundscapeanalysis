#!/bin/bash
if [ -n "$1" ]; then
    prefix=../$1;
else
    prefix='./train_dataset';
fi

cd train_dataset
files=$(ls)
for class in $files; do
    mv $class/skipped_negatives/* $class/not_$class/
    rm -r $class/skipped_negatives
done

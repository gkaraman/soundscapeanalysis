#!/bin/bash
if [ -n "$1" ]; then
    prefix=../$1;
else
    prefix='./train_dataset';
fi

cd $prefix
files=$(ls)
for class in $files; do
    echo $(pwd)
    cd ./$class
    neg=$(ls not_$class | wc -l); 
    pos=$(ls $class/ | wc -l);
    if [ $neg -gt $pos ]; then 
        let dif=$neg-$pos;
        mkdir skipped_negatives
        cd not_$class
        mv $( ls | sort -R | tail -$dif ) ../skipped_negatives/;
        cd ..
    fi;
    cd ..
done


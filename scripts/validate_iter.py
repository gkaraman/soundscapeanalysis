import os,subprocess,sys
import train,parse,validate
import matplotlib.pyplot as plt
from matplotlib.legend_handler import HandlerLine2D
import pylab
FNULL = open(os.devnull, 'w')

def validate_iter(wavfolder="../wav/",NumberOfIterations=4,classValidation=1):
    folders = ["airplanes","animals","birds","machinery","music", "running_water","shouting","sirens-horns","subway-trains" \
                ,"vehicles","voice_(adults)","voice_(children)","walking","waves-sea","wind"]
    resultsfolder = "validation_results"
    #classValidation    = 0          # 1 for Class Validation , 0 for File Validation
    #thresholdValues    = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]
    thresholdValues = [0.5]

    if NumberOfIterations < 1: return
    
    if classValidation == 0:
        l = len(thresholdValues); print "\t\t... File Validation ..."
    else: 
        l = len(folders); print "\t\t... Class Validation ..."

    Precisions = [0.0] * l; 
    Recalls = [0.0] * l; 
    Accuracys = [0.0] * l; 
    F1s = [0.0] * l
    TruePositives = [0] * l; 
    TrueNegatives = [0] * l; 
    FalsePositives = [0] * l; 
    FalseNegatives = [0] * l

    if os.path.exists(resultsfolder): subprocess.call(["rm","-r",resultsfolder])
    subprocess.call(["mkdir",resultsfolder])

    for k in range(1,NumberOfIterations+1):
            sys.stdout = sys.__stdout__
            print "\n\t************* Iteration %d *************"%k
            
            #"""        ###### Uncomment only for debugging reasons...
            # Randomly split into training and validation set
            print "Randomly splitting into training and validation set..."
            subprocess.call(["./split_train_valid.sh",wavfolder],stdout=FNULL, stderr=subprocess.STDOUT)

            # To solve the problem of unbalanced data, use this script
            subprocess.call(["./reduce_negatives.sh"],stdout=FNULL, stderr=subprocess.STDOUT)

            # Create train_dataset folder
            parse.parse_json(1)
            if classValidation == 1:
                # Create validation_dataset folder
                parse.parse_json(2)

            # Train data in train_dataset folder and create SVMs
            print "Training classes - creating SVM files..."
            outfile = open("./%s/Training%d.txt"%(resultsfolder,k), "w")
            sys.stdout = outfile
            train.train_data(1)
            outfile.close()
            #"""

            # Validate data from validation_dataset
            sys.stdout = sys.__stdout__
            print "Validating data from validation_dataset using the SVM files"
            outfile = open("./%s/Validation%d.txt"%(resultsfolder,k), "w")
            sys.stdout = outfile
            print "*************** Validation %d ******************"%k
            if classValidation == 0:
                accuracy , precision , recall, f1 , tps , tns, fps, fns = validate.validate_files("./validation_set/","default",thresholdValues)
            else:        
                accuracy , precision , recall, f1 , tps , tns, fps, fns = validate.validate_classes(2)

            outfile.close()
            Accuracys = [x + y for x, y in zip(Accuracys, accuracy)]
            Precisions = [x + y for x, y in zip(Precisions, precision)]
            Recalls = [x + y for x, y in zip(Recalls, recall)]
            F1s = [x + y for x, y in zip(F1s, f1)]
            TruePositives = [x + y for x, y in zip(TruePositives, tps)]
            TrueNegatives = [x + y for x, y in zip(TrueNegatives, tns)]
            FalsePositives = [x + y for x, y in zip(FalsePositives, fps)]
            FalseNegatives = [x + y for x, y in zip(FalseNegatives, fns)]
            
            if classValidation == 0 and len(thresholdValues) > 1: 
                fig = plt.figure()
                ax = fig.add_axes([0.1, 0.1, 0.6, 0.75])
                ax.plot(thresholdValues, accuracy, marker='o',label='Accuracy')
                ax.plot(thresholdValues, f1,marker='o', label='F1')
                ax.plot(thresholdValues, precision, marker='o',label='Precision')
                ax.plot(thresholdValues, recall, marker='o',label='Recall')
                ax.set_xlabel('Threshold', size=12)
                ax.set_ylabel('Results', size=12)
                ax.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
                plt.title('Validation%d'%k)
                plt.savefig('./%s/Validation%d.png'%(resultsfolder,k))


    # Final Steps
    subprocess.call(["./fix_training_files.sh","%s/"%resultsfolder])
    outfile = open("./%s/FinalResults.txt"%resultsfolder, "w")
    sys.stdout = outfile


    AvgAccuracy = [x/float(NumberOfIterations) for x in Accuracys]
    AvgPrecision = [x/float(NumberOfIterations) for x in Precisions]
    AvgRecall = [x/float(NumberOfIterations) for x in Recalls]
    AvgF1 = [x/float(NumberOfIterations) for x in F1s]

    if classValidation == 0 and len(thresholdValues) > 1: 
        fig = plt.figure()
        ax = fig.add_axes([0.1, 0.1, 0.6, 0.75])
        ax.plot(thresholdValues, AvgAccuracy, marker='o',label='Accuracy')
        ax.plot(thresholdValues, AvgF1,marker='o', label='F1')
        ax.plot(thresholdValues, AvgPrecision, marker='o',label='Precision')
        ax.plot(thresholdValues, AvgRecall, marker='o',label='Recall')
        ax.set_xlabel('Threshold', size=12)
        ax.set_ylabel('Results', size=12)
        ax.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        #plt.show()
        plt.title('Validation Results - Average of %d Iterations'%NumberOfIterations)
        plt.savefig('./%s/Results.png'%resultsfolder)

    print "*************** Results of validating %d times ******************"%NumberOfIterations

    if classValidation == 0:
        print "Threshold Values:",thresholdValues
        #TODO Diagram plotting values for every threshold
        if l > 1:
            maxAcc = max(AvgAccuracy)
            maxPrec = max(AvgPrecision)
            maxRec = max(AvgRecall)
            maxF1 = max(AvgF1)
            maxAccInd = AvgAccuracy.index(maxAcc)
            maxPrecInd = AvgPrecision.index(maxPrec)
            maxRecInd = AvgRecall.index(maxRec)
            maxF1Ind = AvgF1.index(maxF1)

            print "Maximum accuracy = ",maxAcc, " for threshold = ",thresholdValues[maxAccInd]
            print "Maximum precision = ",maxPrec, " for threshold = ",thresholdValues[maxPrecInd]
            print "Maximum recall = ",maxRec, " for threshold = ",thresholdValues[maxRecInd]
            print "Maximum f1 = ",maxF1, " for threshold = ",thresholdValues[maxF1Ind]
            
            if 0.5 in thresholdValues:    
                print "\n\n--- Results for threshold = 0.5 ---"
                i = thresholdValues.index(0.5)
                print "Accuracy  = ",AvgAccuracy[i],"\nPrecision = ",AvgPrecision[i],"\nRecall    = ",AvgRecall[i], "\nF1        = ",AvgF1[i]

        if l==1:
            print "AvgAccuracy : %f\nAvgPrecision: %f\nAvgRecall   : %f\nAvgF1       : %f\n"%(AvgAccuracy[0],AvgPrecision[0],AvgRecall[0],AvgF1[0])
            print "True Positives : ",TruePositives[0],"\tFalse Positives: ",FalsePositives[0],"\nFalse Negatives: ",FalseNegatives[0],"\tTrue Negatives: ",TrueNegatives[0]

    else:
        for f in folders:
            print "\n\n----------------- Class: "+f+" ------------------------"
            ind = folders.index(f)
            print "True Positives  = %d ,\t False Negatives = %d"%(TruePositives[ind],FalseNegatives[ind])
            print "False Positives = %d ,\t True Negatives  = %d"%(FalsePositives[ind],TrueNegatives[ind])

            finalAcc = finalPrec = finalRec = finalF1 = 0.0
            tp = TruePositives[ind]
            tn = TrueNegatives[ind]
            fp = FalsePositives[ind]
            fn = FalseNegatives[ind]


            if tp+fp!=0: finalPrec = tp/(tp+fp)
            if tp+fn!=0: finalRec = tp/(tp+fn)
            if (finalPrec !=0) and (finalRec !=0): finalF1 = 2.0*finalPrec*finalRec/(finalPrec+finalRec)
            if tp+tn+fp+fn !=0 : finalAcc = (tp+tn)/(tp+tn+fp+fn)
            print "-- Micro-Average Results --\nAccuracy : %f\nPrecision: %f\nRecall   : %f\nF1       : %f\n"%(finalAcc,finalPrec,finalRec,finalF1)
            #print "\n-----Macro-Average Results -----\nAvgAccuracy : %f\nAvgPrecision: %f\nAvgRecall   : %f\nAvgF1       : %f\n"%(AvgAccuracy[ind],AvgPrecision[ind],AvgRecall[ind],AvgF1[ind])

        ProcessedClasses = 0
        for k in range(0,len(folders)):
            ProcessedClasses += 1
            if (AvgAccuracy[k] == 0.0) and (AvgPrecision[k] == 0.0) and (AvgRecall[k]==0.0) and (AvgF1[k]==0.0):
                ProcessedClasses -= 1

        AvgAvgAccuracy = sum(AvgAccuracy) / float(l)
        AvgAvgPrecision = sum(AvgPrecision) / float(l)
        AvgAvgRecall = sum(AvgRecall) / float(l)
        AvgAvgF1 = sum(AvgF1) / float(l)
        print "\n\nAverage Results of all processed classes:"
        print "AvgAccuracy : %f\nAvgPrecision: %f\nAvgRecall   : %f\nAvgF1       : %f\n"%(AvgAvgAccuracy,AvgAvgPrecision,AvgAvgRecall,AvgAvgF1)

    outfile.close()

    sys.stdout = sys.__stdout__
    outfile = open("./%s/FinalResults.txt"%resultsfolder, "r")
    text = outfile.read()
    print ""
    print text    
    outfile.close()


def main(argv):
    validate_iter()
    return 0

if __name__ == '__main__':
    main(sys.argv)

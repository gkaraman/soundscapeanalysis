import os,sys,glob,subprocess
from os import listdir
from os.path import isfile, join
from pyAudioAnalysis import audioTrainTest as aT

def train_data(option = 1):
        # In order to train the SVM classifiers, a dataset folder has to be created by "parse.py" script...
        # Train SVM Classifier for the dataset
        destinations = ["./complete_dataset/","./train_dataset/","./validation_dataset/","train_dataset_reduced/"]

        if option == 0:
            if os.path.exists("../svmClassifiers"): subprocess.call(["rm","-r","../svmClassifiers"])
            subprocess.call(["mkdir","../svmClassifiers"])
            outfile = open("../svmClassifiers/TrainingResults.txt","w")
            sys.stdout = outfile

        dest = destinations[option] # in which folder to train data
        print "\n*** Training data in folder: ", dest ," ***\n"
        folders = ["airplanes","animals","birds","machinery","music", "running_water","shouting","sirens-horns","subway-trains" \
                        ,"vehicles","voice_(adults)","voice_(children)","walking","waves-sea","wind"]

        for f in folders:
            classpath = dest+f+"/"
            posFiles = [ file1 for file1 in glob.glob(classpath+f+"/*.npy") ]
            negFiles = [ file1 for file1 in glob.glob(classpath+"not_"+f+"/*.npy") ]

            if ((len(posFiles)<5) or (len(negFiles)<5)):
                print "Class %s has too poor data...\n"%f
                continue

            print "\n\n-------- Processing folder %s -----------"%classpath
            aT.featureAndTrain([dest+"%s/%s"%(f,f),dest+"%s/not_%s"%(f,f)], 1.0, 1.0, aT.shortTermWindow, aT.shortTermStep, "svm", dest+"%s/svm%s"%(f,f))
            if option == 0:
                subprocess.call(["cp",dest+f+"/svm"+f,"../svmClassifiers/"])
                subprocess.call(["cp",dest+f+"/svm"+f+".arff","../svmClassifiers/"])
                subprocess.call(["cp",dest+f+"/svm"+f+"MEANS","../svmClassifiers/"])

        if option == 0 : 
            outfile.close()
            subprocess.call(["./fix_training_files.sh","../svmClassifiers/"])
            sys.stdout = sys.__stdout__

        return


def main(argv):
    train_data()
    return

if __name__ == '__main__':
    main(sys.argv)

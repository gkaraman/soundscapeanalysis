#!/bin/bash
if [[ -n "$1" ]]; then
    prefix=$1;
else
    prefix='./';
fi

for f in $prefix\Training*.txt; do 
    cat $f | grep -v Param | grep -v Feat > tmp; rm $f; mv tmp $f;
done

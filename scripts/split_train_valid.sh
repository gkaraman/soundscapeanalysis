#!/bin/bash

if [ -n "$1" ]; then
    prefix=$1;
else
    prefix='../wav';
fi

rm -r train_set/
rm -r validation_set/
mkdir train_set validation_set
ln $prefix/* ./train_set/
cd train_set/
wavFiles=$(ls *.wav |sort -R |tail -$(expr $(ls *.wav | wc -l) / 5))
for f in $wavFiles; do 
    if [ ! -f $f\_tags.npy ]; then
        echo File $f\_tags.npy not found
    else
        mv $f ../validation_set
        mv $f.npy ../validation_set; 
        mv $f\_tags.npy ../validation_set;
    fi

done

import numpy,json
import sys,os,subprocess
from pyAudioAnalysis import audioAnalysis
#from pprint import pprint

def parse_json(option=0, sourcefolder='../wav/',json_file='../data.json'):
    FNULL = open(os.devnull, 'w')
    sources = ["../wav/","./train_set/","./validation_set/"]
    destinations = ["./complete_dataset/","./train_dataset/","./validation_dataset/"]
  
    if option == 0:
        source = sourcefolder
    else:
        source = sources[option]  # from which set to take the wav files
    dest = destinations[option]   # which folder (dataset) to create

    # Preprocessing
    if (option == 0):
        print "avconv : Processing wav files to be compatible..."
        subprocess.call(["./make_compatible_wav.sh",sourcefolder],stdout=FNULL, stderr=subprocess.STDOUT) # Make the wav files compatible
        print "Feature Extraction..."
        f = open(os.devnull, 'w')
        sys.stdout = f         
        audioAnalysis.featureExtractionDirWrapper(sourcefolder,1.0,1.0,0.05,0.05)
        f.close()        
        sys.stdout = sys.__stdout__
        #subprocess.call(["./feature_extraction.sh",sourcefolder],stdout=FNULL, stderr=subprocess.STDOUT)  # Feature Extraction
    

    if os.path.exists(dest): subprocess.call(["rm","-r",dest])
    dTags = ["airplanes","animals","birds","machinery","music", \
                    "running water","shouting","sirens-horns","subway-trains" \
                    ,"vehicles","voice (adults)","voice (children)","walking" \
                    ,"waves-sea","wind"]

    subprocess.call(["mkdir",dest])
    subprocess.call(["mkdir",dest+"otherTags"])
    for t in dTags:
        t = t.replace(' ','_')
        subprocess.call(["mkdir",dest+t])
        subprocess.call(["mkdir",dest+t+"/"+t])
        subprocess.call(["mkdir",dest+t+"/not_"+t])

    subprocess.call(["mkdir",dest+"quality"])
    for t in range(1,6):
        subprocess.call(["mkdir",dest+"quality/%d"%t])

    # Parsing json file
    with open(json_file) as data_file:
        data = json.load(data_file)
        #pprint(data)

    l = len(data)
    print "Parsing json database..."
    hit = 0
    missed_files = []

    for key in data:
        #print key
        rec = data[key]
        #annotator = rec["annotator"]
        csvFile = rec["csvFile"]
        #deviceId = rec["deviceId"]
        #geo = rec["geo"]
        soundscapeQuality = rec["soundscapeQuality"]
        wavFile = csvFile.replace("csv","wav")
        ret = subprocess.call(["ln",source+wavFile, dest+"quality/%s/%s"%(soundscapeQuality,wavFile)],stdout=FNULL, stderr=subprocess.STDOUT)
        #subprocess.call(["ln",source+wavFile+'.npy', dest+"quality/%s/%s.npy"%(soundscapeQuality,wavFile)],stdout=FNULL, stderr=subprocess.STDOUT)

        if not ret: hit+=1
        else:
            missed_files.append(wavFile)
            continue

        userTags = []
        if "tags" in rec:
            userTags = rec["tags"]
        if option == 0 :
            saveTags = [x.replace('/','-').replace(' ','_') for x in userTags]
            numpy.save(sourcefolder+wavFile+"_tags",saveTags)
            numpy.savetxt(sourcefolder+wavFile+"_tags.txt",saveTags,delimiter=" ", fmt="%s")

        for t in dTags:
            t2 = t.replace('-','/') # in order to match correctly
            t = t.replace(' ','_')  # for correct folder names
            if t2 in userTags:
                subprocess.call(["ln",source+wavFile, dest+t+"/"+t],stdout=FNULL, stderr=subprocess.STDOUT)
                subprocess.call(["ln",source+wavFile+'.npy', dest+t+"/"+t],stdout=FNULL, stderr=subprocess.STDOUT)
            else:
                subprocess.call(["ln",source+wavFile, dest+t+"/not_"+t],stdout=FNULL, stderr=subprocess.STDOUT)
                subprocess.call(["ln",source+wavFile+'.npy', dest+t+"/not_"+t],stdout=FNULL, stderr=subprocess.STDOUT)

        # If user has given a new tag, make a new folder inside otherTags
        for t in userTags:
            t = t.replace('/','-')
            if t not in dTags:
                t = t.replace(' ','_')
                subprocess.call(["mkdir",dest+"otherTags/%s"%t], stdout=FNULL, stderr=subprocess.STDOUT)
                subprocess.call(["ln",source+wavFile, dest+"otherTags/%s"%t])
                subprocess.call(["ln",source+wavFile+'.npy', dest+"otherTags/%s"%t])

    print "Successfully matched %d out of %d recordings into %s\n" %(hit,l,dest)
    
    if option == 0:
        print "Missing wav files: "
        for m in missed_files: print m

    return 

def main(argv):
    parse_json()
    return 

if __name__ == '__main__':
    main(sys.argv)


import os,subprocess,sys
import parse,train,validate,validate_file,validate_iter

def trainsvm(wavFolder="../wav/",db="../data.json",NumberOfIterations=2):
    """ trainsvm: trains SVM Classifiers
        INPUT:
            wavFolder: the folder containing the training audio files. 
            db: the database in .json format.
            NumberOfIterations: number of iterations for validating the classifiers by splitting
            to training data (70%) and validation data (30%). If NumberOfIterations is zero, then
            there will be no validation stage.
    """
    if not wavFolder.endswith('/'): wavFolder += '/' # Fix audio folder to have proper folder form 
    if not os.path.exists(wavFolder):
        print "[ERROR] wav folder %s doesn't exist..."%wavFolder
        return

    if not os.path.exists(db):
        print "[ERROR] database file %s doesn't exist..."%db
        return

    # Preparing the files...
    parse.parse_json(0,wavFolder,db)

    # Training the SVM Models
    print "Training SVM Models..."
    print "The SVM Classifiers have been trained and placed in 'SVMClassifiers' folder..."
 
    # Validating the SVM Models
    if NumberOfIterations > 0:
        print "\n\nValidating the SVM Classifiers..."
        validate_iter.validate_iter(wavFolder,NumberOfIterations,0)

    return

def applysvm(testdata="./train_set",svmfolder="../defaultClassifiers/"):
    """ applysvm: classifies test data using already trained SVM Classifiers.
    INPUT
    testdata: an audio file or a folder of audio files for classification.
    svmfolder: the folder containing the SVM files of the trained SVM Classifiers.

    """
    if not os.path.exists(svmfolder):
        print "[ERROR] Wrong SVM folder path..."
        return

    if testdata.lower().endswith(('.wav','.mp3')):
        # One-File Classification
        [groundtruthtags,predictedtags] = validate_file.validate_file(testdata,svmfolder,0.5)
        if groundtruthtags == -1 or predictedtags == -1: return
        if groundtruthtags != []:
            print "Ground Truth Tags :",
            for i in groundtruthtags: print i ,
            print ""
        print "Predicted Tags    :",
        for i in predictedtags: print i,
        print ""
        return

    # Folder-Of-Files Classification
    if not os.path.exists(testdata):
        print "[ERROR] The folder %s does not exist..."%testdata
        return

    validate.validate_files(testdata,svmfolder,[0.5])

    return


def main(argv):
    trainSVM()
    return 0

if __name__ == '__main__':
    main(sys.argv)

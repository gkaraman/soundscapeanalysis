import sys,glob
import numpy
import time
import os
from pyAudioAnalysis import audioFeatureExtraction as aF
from pyAudioAnalysis import audioTrainTest as aT
from pyAudioAnalysis import audioBasicIO
from os import listdir
from os.path import isfile, join



def validate_file(validationfile,svmpath="default",probThreshld=0.5):
    folders = ["airplanes","animals","birds","machinery","music", \
                    "running_water","shouting","sirens-horns","subway-trains" \
                    ,"vehicles","voice_(adults)","voice_(children)","walking" \
                    ,"waves-sea","wind"]

    maxF1threshold = [0.1,0.1,0.3,0.4,0.4,0.1,0.1,0.1,0.3,0.5,0.2,0.1,0.2,0.1,0.3]
    #sources = ["./complete_dataset/","./train_dataset/","./validation_dataset/","train_dataset_reduced/"]
    stWin = 0.05; stStep = 0.05; mtWin = 1.0; mtStep = 1.0
    if not os.path.exists(validationfile):
        print "[ERROR] Wrong file name..."
        return -1,-1
    
    defaultsvmpath = 0
    if svmpath == "default": 
        svmsource = "./train_dataset/"            # where to find the svm files: default = "./train_dataset/    
        svmpath = svmsource        
        defaultsvmpath = 1   
    
    if not svmpath.endswith('/'): svmpath += '/' # Fix svmpath to have proper folder form
        
    if not os.path.exists(svmpath):
        print "[ERROR] Wrong SVM path..."
        return -1,-1

     
    modelType = "svm"
    l = len(folders)

    inputFile = validationfile+".npy"

    if os.path.exists(inputFile):
        MidTermFeatures = numpy.load(inputFile)
        MidTermFeatures = MidTermFeatures.mean(axis=1)                      # long term averaging of mid-term statistics
    else:
        # Feature extraction
        [Fs, x] = audioBasicIO.readAudioFile(validationfile)        # read audio file    and convert to mono
        x = audioBasicIO.stereo2mono(x)
        [MidTermFeatures, s] = aF.mtFeatureExtraction(x, Fs, mtWin * Fs, mtStep *   Fs, round(Fs * stWin), round(Fs * stStep))
        MidTermFeatures = MidTermFeatures.mean(axis=1)        # long term averaging of mid-term statistics
    
    if not os.path.exists(validationfile+'_tags.npy'):
        print "No tags file..."
        groundtruthtags = []
    else:
        groundtruthtags = numpy.load(validationfile+'_tags.npy')
    
    predictedtags = []
    predictedtagsprobabilities = []
    tp = fp = fn = tn = 0
    #print "Available classes for classifying:"
    for ind,f in enumerate(folders):
        if defaultsvmpath == 1: svmpath = svmsource+f+"/"
        modelName = "svm"+f

       
        if not os.path.isfile(svmpath+modelName):
            #print "[SVM ERROR] %s not found in folder %s\n\n"%(modelName,svmpath)
            continue
        #else: print modelName,

        # Load classifiers:
        if modelType == 'svm':
            [Classifier, MEAN, STD, classNames, mtWin, mtStep, stWin, stStep, computeBEAT] = aT.loadSVModel(svmpath+modelName)
        elif modelType == 'knn':
            [Classifier, MEAN, STD, classNames, mtWin, mtStep, stWin, stStep, computeBEAT] = at.loadKNNModel(svmpath+modelName)


        curFV = (MidTermFeatures - MEAN) / STD                              # normalization
        [Result, P] = aT.classifierWrapper(Classifier, modelType, curFV)    # classification

        positiveInd = classNames.index(f)
        #if P[positiveInd] > probThreshld:
        if P[positiveInd] > 0.5: predictedtags.append(f)
        #elif P[positiveInd] > 0.5:
        #    print "Didn't choose",f, ". Prob =",P[positiveInd], ". Thres =",maxF1threshold[ind]


        
        """  
        if (classNames[int(Result)] == f):
            #predictedtags.append(f)
            # Thresholding            
            if P[int(Result)] > probThreshld:
                predictedtags.append(f)
            else: print "Didn't choose",f, ". Prob =",P[int(Result)], ". Thres = ",probThreshld
        """
            #if f in groundtruthtags: tp += 1
            #else: fp +=1
        #else:
            #if f in groundtruthtags: fn += 1
            #else: tn +=1

        predictedtagsprobabilities.append(P[int(Result)])

    """
    if not defaultsvmpath: 
        Precision = Recall = F1 = 0.0
        if tp+fp != 0: Precision = tp / float(tp+fp)
        if tp+fn != 0: Recall = tp / float(tp+fn)
        if Precision + Recall !=0: F1 = 2 * (Precision*Recall)/(Precision + Recall)
        print "File: "+validationfile
        print "Ground Truth Tags :" ,
        for i in groundtruthtags: print i ,
        print "\nPredicted Tags    :",
        for i in predictedtags: print i,
        print "\nPrecision = ",Precision,"\nRecall    = ",Recall, "\nF1        = ",F1
        #return Accuracys,Precisions,Recalls,F1s,TPs,TNs,FPs,FNs
    """
    #print ""
    return groundtruthtags,predictedtags

def main(argv):
    validate_file()
    return

if __name__ == '__main__':
    main(sys.argv)

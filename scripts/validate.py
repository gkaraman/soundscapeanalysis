import sys,glob
import numpy
import time
import os
from pyAudioAnalysis import audioFeatureExtraction as aF
from pyAudioAnalysis import audioTrainTest as aT
from pyAudioAnalysis import audioBasicIO
from os import listdir
from os.path import isfile, join
import validate_file as vF


def validate_files(validation_path="./validation_set",svmpath="default",thresholdValues = [0.5]):
    folders = ["airplanes","animals","birds","machinery","music", \
                    "running_water","shouting","sirens-horns","subway-trains" \
                    ,"vehicles","voice_(adults)","voice_(children)","walking" \
                    ,"waves-sea","wind"]

    #sources = ["./complete_dataset/","./train_dataset/","./validation_set/","train_dataset_reduced/"]
    l = len(thresholdValues)
    TruePos = [0]*l
    TrueNeg = [0]*l
    FalsePos = [0]*l
    FalseNeg = [0]*l
    AccuracysperThres = [0]*l
    PrecisionsperThres = [0]*l
    RecallsperThres = [0]*l
    F1perThres = [0]*l
    groundtruthexists = 0
    #validation_path = sources[option] # which are the validation data: default = "./validation_dataset"
    
    tagfiles = [ file1 for file1 in glob.glob(validation_path+"/*.wav") ]
    tps = tns = fps = fns = 0
    
    for ind,t in enumerate(thresholdValues):
        print "\n------- Threshold = ",t," -------"
        for f in tagfiles:
            print "\nFile "+f
            tp=tn=fp=fn=0
            [groundtruthtags,predictedtags] = vF.validate_file(f,svmpath,t)
            if groundtruthtags == -1 or predictedtags == -1: continue
            for fol in folders:
                if fol in groundtruthtags:
                    if fol in predictedtags: tp += 1
                    else: fn +=1
                else:
                    if fol in predictedtags: fp += 1
                    else: tn +=1   
            tps += tp
            fps += fp
            tns += tn
            fns += fn
            Accuracy = Precision = Recall = F1 = 0.0
            if tp+fp+tn+fn !=0: Accuracy = (tp+tn)/ float(tp+fp+tn+fn)
            if tp+fp != 0: Precision = tp / float(tp+fp)
            if tp+fn != 0: Recall = tp / float(tp+fn)
            if Precision + Recall !=0: F1 = 2 * (Precision*Recall)/(Precision + Recall)

            groundtruthtags.sort()
            predictedtags.sort()
            if groundtruthtags != []:
                print "Ground Truth Tags :" ,
                for i in groundtruthtags:
                    if i in folders: print i,"|",
                print ""
            print "Predicted Tags    :",
            for i in predictedtags: print i,"|",
            print ""
            if groundtruthtags != []: 
                print "Precision = ",Precision,"\nRecall    = ",Recall, "\nF1        = ",F1
                groundtruthexists = 1
            #return Accuracys,Precisions,Recalls,F1s,TPs,TNs,FPs,FNs
        if groundtruthexists:
            Accuracys = Precisions = Recalls = F1s = 0.0
            if tps+fps+tns+fns !=0: Accuracys = (tps+tns)/ float(tps+fps+tns+fns)
            if tps+fps != 0: Precisions = tps / float(tps+fps)
            if tps+fns != 0: Recalls = tps / float(tps+fns)
            if Precisions + Recalls !=0: F1s = 2 * (Precisions*Recalls)/(Precisions + Recalls)
            print "\n\nAverage Results:"    
            print "Accuracy  = ",Accuracys,"\nPrecision = ",Precisions,"\nRecall    = ",Recalls, "\nF1        = ",F1s
            print "True Positives : ",tps,"\tFalse Positives: ",fps,"\nFalse Negatives: ",fns,"\tTrue Negatives : ",tns
            TruePos[ind] = tps
            TrueNeg[ind] = tns
            FalsePos[ind] = fps
            FalseNeg[ind] = fns
            AccuracysperThres[ind] = Accuracys
            PrecisionsperThres[ind] = Precisions
            RecallsperThres[ind] = Recalls
            F1perThres[ind] = F1s
    
    if len(thresholdValues) > 1 and groundtruthexists:
        maxAcc = max(AccuracysperThres)
        maxPrec = max(PrecisionsperThres)
        maxRec = max(RecallsperThres)
        maxF1 = max(F1perThres)
        maxAccInd = AccuracysperThres.index(maxAcc)
        maxPrecInd = PrecisionsperThres.index(maxPrec)
        maxRecInd = RecallsperThres.index(maxRec)
        maxF1Ind = F1perThres.index(maxF1)
        print "\n\nMaximum accuracy = ",maxAcc, " for threshold = ",thresholdValues[maxAccInd]
        print "Maximum precision = ",maxPrec, " for threshold = ",thresholdValues[maxPrecInd]
        print "Maximum recall = ",maxRec, " for threshold = ",thresholdValues[maxRecInd]
        print "Maximum f1 = ",maxF1, " for threshold = ",thresholdValues[maxF1Ind]
        return AccuracysperThres , PrecisionsperThres , RecallsperThres, F1perThres , TruePos , TrueNeg, FalsePos, FalseNeg
    else:
        return AccuracysperThres , PrecisionsperThres , RecallsperThres, F1perThres , TruePos , TrueNeg, FalsePos, FalseNeg




def validate_classes(option=2):
    folders = ["airplanes","animals","birds","machinery","music", \
                    "running_water","shouting","sirens-horns","subway-trains" \
                    ,"vehicles","voice_(adults)","voice_(children)","walking" \
                    ,"waves-sea","wind"]

    maxF1threshold = [0.0,0.0,0.3,0.4,0.4,0.0,0.0,0.0,0.3,0.4,0.2,0.0,0.2,0.0,0.3]

    sources = ["./complete_dataset/","./train_dataset/","./validation_dataset/","train_dataset_reduced/"]
    option = 2
    validation_path = sources[option] # which are the validation data: default = "./validation_dataset"
    if not os.path.exists(validation_path):
        print "[ERROR] path %s does not exist..."%validation_path
        return -1,-1,-1,-1,-1,-1,-1,-1

    svmsource = sources[1]            # from where to take the svm files: default = "./train_dataset/"
    if not os.path.exists(svmsource):
        print "[ERROR] path %s does not exist..."%svmsource
        return -1,-1,-1,-1,-1,-1,-1,-1

    modelType = "svm"
    l = len(folders)
    Precisions = [0.0] * l
    Recalls = [0.0] * l
    Accuracys = [0.0] * l
    F1s = [0.0] * l
    TPs = [0] * l
    TNs = [0] * l
    FPs = [0] * l
    FNs = [0] * l
    AvgPrecision = AvgRecall = AvgAccuracy = AvgF1 = 0.0
    ProcessedClasses = 0
    for ind,f in enumerate(folders):

        print "\n----------------- Folder: "+f+" ------------------------"
        classpath = validation_path+f+"/"
        svmpath = svmsource+f+"/"
        modelName = "svm"+f

        #posFiles = [ d for d in listdir(classpath+f) if isfile(join(classpath+f,d)) ]
        #negFiles = [ d for d in listdir(classpath+"not_"+f) if isfile(join(classpath+"not_"+f,d)) ]

        posFiles = [ file1 for file1 in glob.glob(classpath+f+"/*.npy") ]
        negFiles = [ file1 for file1 in glob.glob(classpath+"not_"+f+"/*.npy") ]
        
        print "Groundtruth Information:"
        print "-- Positive Files:" , len(posFiles)
        print "-- Negative Files:" , len(negFiles)

        if (len(posFiles)<1 or len(negFiles)<1):
            print "Very poor data in class %s\n"%f
            continue

        if not os.path.isfile(svmpath+modelName):
            print "[SVM ERROR] %s not found in folder %s\n\n"%(modelName,svmpath)
            continue

        # Load classifiers:
        if modelType == 'svm':
            [Classifier, MEAN, STD, classNames, mtWin, mtStep, stWin, stStep, computeBEAT] = aT.loadSVModel(svmpath+modelName)
        elif modelType == 'knn':
            [Classifier, MEAN, STD, classNames, mtWin, mtStep, stWin, stStep, computeBEAT] = at.loadKNNModel(svmpath+modelName)

        tp=tn=fp=fn=0.0

        for mp3file in posFiles:
            #inputFile = classpath+f+"/"+mp3file
            inputFile = mp3file
            #[Result,P,classNames] = fileClassification(mp3filePath,modelName,modelType)

            if not os.path.isfile(inputFile):
                print "fileClassification: wav file %s not found!"%inputFile
                continue

            MidTermFeatures = numpy.load(inputFile)
            MidTermFeatures = MidTermFeatures.mean(axis=1)                      # long term averaging of mid-term statistics

            curFV = (MidTermFeatures - MEAN) / STD                              # normalization
            [Result, P] = aT.classifierWrapper(Classifier, modelType, curFV)    # classification

            positiveInd = classNames.index(f)
            if P[positiveInd] > 0.5: tp+=1
            else: fn +=1

        for mp3file in negFiles:
            #inputFile = classpath+"not_"+f+"/"+mp3file
            inputFile = mp3file
            #[Result,P,classNames] = fileClassification(mp3filePath,modelName,modelType)
            if not os.path.isfile(inputFile):
                print "fileClassification: wav file %s not found!"%inputFile
                continue
            
            MidTermFeatures = numpy.load(inputFile)
            MidTermFeatures = MidTermFeatures.mean(axis=1)                      # long term averaging of mid-term statistics
            if computeBEAT:
                [beat, beatConf] = aF.beatExtraction(s, stStep)
                MidTermFeatures = numpy.append(MidTermFeatures, beat)
                MidTermFeatures = numpy.append(MidTermFeatures, beatConf)
            curFV = (MidTermFeatures - MEAN) / STD                              # normalization
            [Result, P] = aT.classifierWrapper(Classifier, modelType, curFV)    # classification

            #print "Result :" ,Result , "\nProbability: ", P, " ",classNames[int(Result)]
            positiveInd = classNames.index(f)
            if P[positiveInd] > 0.5: fp+=1
            else: tn +=1

        Precision = Recall = F1 = 0.0;
        if tp+fp!=0: Precision = tp/(tp+fp)
        if tp+fn!=0: Recall = tp/(tp+fn)
        if (Precision !=0) and (Recall !=0): F1 = 2.0*Precision*Recall/(Precision+Recall)
        Accuracy = (tp+tn)/(tp+tn+fp+fn)
        print "True Positives  = %d ,\t False Negatives = %d"%(tp,fn)
        print "False Positives = %d ,\t True Negatives  = %d"%(fp,tn)
        print "Accuracy : %f\nPrecision: %f\nRecall   : %f\nF1       : %f "%(Accuracy,Precision,Recall,F1)
        AvgAccuracy += Accuracy
        AvgPrecision += Precision
        AvgRecall += Recall
        AvgF1 += F1
        ProcessedClasses += 1

        Accuracys[ind] = Accuracy            
        Precisions[ind] = Precision
        Recalls[ind] = Recall
        F1s[ind] = F1
        TPs[ind] = tp
        TNs[ind] = tn
        FPs[ind] = fp
        FNs[ind] = fn

    # Average Results
    if ProcessedClasses != 0:
        print "\n\n----------------- Average Results -----------------"
        AvgAccuracy = AvgAccuracy / ProcessedClasses
        AvgPrecision = AvgPrecision / ProcessedClasses
        AvgRecall = AvgRecall / ProcessedClasses
        AvgF1 = AvgF1 / ProcessedClasses
        print "AvgAccuracy : %f\nAvgPrecision: %f\nAvgRecall   : %f\nAvgF1       : %f\n"%(AvgAccuracy,AvgPrecision,AvgRecall,AvgF1)
    return Accuracys,Precisions,Recalls,F1s,TPs,TNs,FPs,FNs




def main(argv):
    #validate_files()
    validate_classes()
    return 0

if __name__ == '__main__':
    main(sys.argv)

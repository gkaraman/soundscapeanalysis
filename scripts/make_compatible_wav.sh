#!/bin/bash

if [ -n "$1" ]; then
    prefix=$1;
else
    prefix='../wav';
fi
rm -r ../tmpwav
mkdir ../tmpwav
cd $prefix
for f in *.wav; do avconv -i $f ../tmpwav/$f -y; done;
cd .. 
rm -r $prefix
mv ./tmpwav $prefix

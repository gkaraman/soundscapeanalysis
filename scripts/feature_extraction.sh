#!/bin/bash
if [ -n "$1" ]; then
    prefix=$1;
else
    prefix='../wav';
fi

cd $prefix
for f in *.wav; do python /home/jk/EMP/dhmokritos/pyAudioAnalysis/audioAnalysis.py featureExtractionFile -i $f -mw 1.0 -ms 1.0 -sw 0.050 -ss 0.050 -o $f; done

